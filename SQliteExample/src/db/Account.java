package db;

import java.io.Serializable;

public class Account implements Serializable{
	
	int id;
	String name;
	String sdt;
	 
	public Account()
	{
		
	}

	public Account(int id, String name, String sdt) {
		super();
		this.id = id;
		this.name = name;
		this.sdt = sdt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", name=" + name + ", sdt=" + sdt + "]";
	}
	
	
}
