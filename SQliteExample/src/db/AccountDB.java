package db;

import java.io.IOException; 
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context; 
import android.database.Cursor; 
import android.database.SQLException; 
import android.database.sqlite.SQLiteDatabase; 
import android.util.Log; 

public class AccountDB<DataBaseHelper>  
{ 
	protected static final String TAG = "DataAdapter"; 
	protected static final String MY_TABLE = "mytable"; 

	private final Context mContext; 
	private SQLiteDatabase mDb; 
	private MyDataBaseHelper mDbHelper; 
	public AccountDB (Context context)  
	{ 
		this.mContext = context; 
		mDbHelper = new MyDataBaseHelper(mContext); 
	} 
	
	public AccountDB  createDatabase() throws SQLException  
	{ 
		try  
		{ 
			mDbHelper.createDataBase(); 
		}  
		catch (IOException mIOException)  
		{ 
			Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase"); 
			throw new Error("UnableToCreateDatabase"); 
		} 
		return this; 
	} 

	public AccountDB  open() throws SQLException  
	{ 
		try  
		{ 
			mDbHelper.openDataBase(); 
			mDbHelper.close(); 
			mDb = mDbHelper.getWritableDatabase(); 
		}  
		catch (SQLException mSQLException)  
		{ 
			Log.e(TAG, "open >>"+ mSQLException.toString()); 
			throw mSQLException; 
		} 
		return this; 
	} 
	///close database
	public void close()  
	{ 
		mDbHelper.close(); 
	} 

	
	//Get data voi Account
	public Account getAccountID(int id) 
	{ 
		Account resut=new Account();
		String sql ="select *from "+MY_TABLE+" where id = "+id;//"SELECT id  
		Cursor mCur = mDb.rawQuery(sql,null);

		if (mCur!=null) 
		{ 
			mCur.moveToNext(); 
		} 
		resut.setId(mCur.getInt(mCur.getColumnIndex("id")));
		resut.setName(mCur.getString(mCur.getColumnIndex("name")));
		resut.setSdt(mCur.getString(mCur.getColumnIndex("sdt")));
	
		Log.d("result : ", resut.toString());
		return resut;
	}

	
	 
	 
	//update
	public void updatetAccount(Account p) 
	{ 
		
		ContentValues values = new ContentValues();
		mDbHelper.getWritableDatabase();
		values.put("id",p.getId());
		values.put("name", p.getName());
		values.put("sdt", p.getSdt());

		mDb.update(MY_TABLE, values,"id"+" = "+p.getId(), null);
		Log.d("update : ", "update thanh cong" +p.toString());
		
	}
		 
	
} 


